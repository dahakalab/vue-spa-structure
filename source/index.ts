import Vue from 'vue';
import VueRouter from 'vue-router';
import Pages from './pages/Pages.vue';
import Plugins from './plugins';

const rootEl = document.querySelector('.root');

Vue.use(VueRouter);
Vue.use(Plugins);

if (rootEl) {
  new Vue({
    el: rootEl,
    render: h => h(Pages),
  });
}
