import Vue from 'vue';
import loadAssets from './loadAssets';
import constants from './constants';

const install = (currentVue: typeof Vue) => {
  Object.defineProperty(currentVue.prototype, '$constants', {
    value: constants,
  });

  Object.defineProperty(currentVue.prototype, '$loadAssets', {
    value: loadAssets,
  });
};

class Plugins {
  static install: (one: typeof Vue) => void;
}

Plugins.install = install;

export default Plugins;
