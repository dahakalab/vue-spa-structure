import Vue from 'vue';
import { Constants } from '~/types/plugins/constants';

interface Methods {
  [key: string]: Function;
}

declare module 'vue/types/vue' {
  interface Vue {
    $constants: Constants;
    $loadAssets: Function;
    // $api: NuxtAxiosInstance;
    // $moment: typeof moment;
    methods?: Methods;
  }
}
